import os
import unittest   #想使用unittest框架，首先要引入unittest 包                  

from appium import webdriver    #导入webdriver库
from appium.webdriver.common.touch_action import touch_action	#导入touch_action库，使得可以玩按成触摸功能
from appium.webdriver.common.multi_action import MultiAction

from time import sleep  #从time模块中引入sleep函数，使用sleep函数可以让程序休眠（推迟调用线程的运行）

# Returns abs path relative to this file and not cwd
PATH = lambda p: os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)
#ComplexAndroidTests类继承unittest.TestCase类，从TestCase类继承是告诉unittest模块的方式，这是一个测试案例
class ComplexAndroidTests(unittest.TestCase):
    #setUp用于设置初始化的部分，在测试用例执行前，这个方法中的函数将先被调用
    def setUp(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'                                        #platformName：要测试的手机操作系统
        desired_caps['platformVersion'] = '4.2'                                         #platformVersion：系统的版本
        desired_caps['deviceName'] = 'Android Emulator'                                 #deviceName：手机或模拟器的类型
        desired_caps['app'] = PATH(                                                     #app：.apk文件所在的本地绝对路径或者远程路径，Appium会先尝试安装路径对应的应用在适当的真机或模拟器上
            '../../../sample-code/apps/ApiDemos/bin/ApiDemos-debug.apk'
        )

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)    #本地appium端口地址

    #tearDown方法在每个测试方法执行后调用，这个地方做所有清理工作，此处执行的是退出
    def tearDown(self):
        self.driver.quit()

    #定义一个定位元素的方法
    def test_find_elements(self):
        # pause a moment, so xml generation can occur
        sleep(2)

        els = self.driver.find_elements_by_xpath('//android.widget.TextView')           #
        self.assertEqual('API Demos', els[0].text)

        el = self.driver.find_element_by_xpath('//android.widget.TextView[contains(@text, "Animat")]')
        self.assertEqual('Animation', el.text)

        el = self.driver.find_element_by_accessibility_id("App")
        el.click()

        els = self.driver.find_elements_by_android_uiautomator('new UiSelector().clickable(true)')
        # there are more, but at least 10 visible
        self.assertLess(10, len(els))
        # the list includes 2 before the main visible elements
        self.assertEqual('Action Bar', els[2].text)

        els = self.driver.find_elements_by_xpath('//android.widget.TextView')
        self.assertLess(10, len(els))
        self.assertEqual('Action Bar', els[1].text)
    #文本滚动的方法
    def test_scroll(self):
        sleep(2)
        els = self.driver.find_elements_by_xpath('//android.widget.TextView')
        self.driver.scroll(els[7], els[3])

        el = self.driver.find_element_by_accessibility_id('Views')

    #测试：在API Demos的'Graphics/Touch Panit'下画出一个笑脸
    def test_smiley_face(self):
        self.driver.find_element_by_accessibility_id('Graphics').click()

        els = self.driver.find_elements_by_class_name('android.widget.TextView')
        self.driver.scroll(els[len(els)-1], els[0])

        el = None
        try:
            el = self.driver.find_element_by_accessibility_id('Touch Paint')
        except Exception as e:
            els = self.driver.find_elements_by_class_name('android.widget.TextView')
            self.driver.scroll(els[len(els)-1], els[0])

        if el is None:
            el = self.driver.find_element_by_accessibility_id('Touch Paint')

        el.click()

        # 画笑脸，先画两个眼睛然后再画嘴，每次画点或线之前，先调用TouchAction()
        e1 = TouchAction()
        e1.press(x=150, y=100).release()

        e2 = TouchAction()
        e2.press(x=250, y=100).release()

        smile = TouchAction()
        smile.press(x=110, y=200) \
            .move_to(x=1, y=1) \
            .move_to(x=1, y=1) \
            .move_to(x=1, y=1) \
            .move_to(x=1, y=1) \
            .move_to(x=1, y=1) \
            .move_to(x=2, y=1) \
            .move_to(x=2, y=1) \
            .move_to(x=2, y=1) \
            .move_to(x=2, y=1) \
            .move_to(x=2, y=1) \
            .move_to(x=3, y=1) \
            .move_to(x=3, y=1) \
            .move_to(x=3, y=1) \
            .move_to(x=3, y=1) \
            .move_to(x=3, y=1) \
            .move_to(x=4, y=1) \
            .move_to(x=4, y=1) \
            .move_to(x=4, y=1) \
            .move_to(x=4, y=1) \
            .move_to(x=4, y=1) \
            .move_to(x=5, y=1) \
            .move_to(x=5, y=1) \
            .move_to(x=5, y=1) \
            .move_to(x=5, y=1) \
            .move_to(x=5, y=1) \
            .move_to(x=5, y=0) \
            .move_to(x=5, y=0) \
            .move_to(x=5, y=0) \
            .move_to(x=5, y=0) \
            .move_to(x=5, y=0) \
            .move_to(x=5, y=0) \
            .move_to(x=5, y=0) \
            .move_to(x=5, y=0) \
            .move_to(x=5, y=-1) \
            .move_to(x=5, y=-1) \
            .move_to(x=5, y=-1) \
            .move_to(x=5, y=-1) \
            .move_to(x=5, y=-1) \
            .move_to(x=4, y=-1) \
            .move_to(x=4, y=-1) \
            .move_to(x=4, y=-1) \
            .move_to(x=4, y=-1) \
            .move_to(x=4, y=-1) \
            .move_to(x=3, y=-1) \
            .move_to(x=3, y=-1) \
            .move_to(x=3, y=-1) \
            .move_to(x=3, y=-1) \
            .move_to(x=3, y=-1) \
            .move_to(x=2, y=-1) \
            .move_to(x=2, y=-1) \
            .move_to(x=2, y=-1) \
            .move_to(x=2, y=-1) \
            .move_to(x=2, y=-1) \
            .move_to(x=1, y=-1) \
            .move_to(x=1, y=-1) \
            .move_to(x=1, y=-1) \
            .move_to(x=1, y=-1) \
            .move_to(x=1, y=-1)
        smile.release()

        ma = MultiAction(self.driver)
        ma.add(e1, e2, smile)
        ma.perform()

        # 这样我们就可以看到笑脸了
        sleep(10)


if __name__ == '__main__':
    #定义一个单元测试容器，将测试用例放入测试容器中
    suite = unittest.TestLoader().loadTestsFromTestCase(ComplexAndroidTests)
    #自动进行测试
    unittest.TextTestRunner(verbosity=2).run(suite)
