import os
from time import sleep

import unittest                                                             #想使用unittest框架，首先要引入unittest 包

from appium import webdriver                                                #导入webdriver库

# Returns abs path relative to this file and not cwd
PATH = lambda p: os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)

#SimpleAndroidTests类继承unittest.TestCase类，从TestCase类继承是告诉unittest模块的方式，这是一个测试案例
class SimpleAndroidTests(unittest.TestCase):                                
    #setUp用于设置初始化的部分，在测试用例执行前，这个方法中的函数将先被调用
    def setUp(self):                                                        
        desired_caps = {}
        desired_caps['platformName'] = 'Android'                            #platformName：要测试的手机操作系统
        desired_caps['platformVersion'] = '4.2'                             #platformVersion：系统的版本
        desired_caps['deviceName'] = 'Android Emulator'                     #deviceName：手机或模拟器的类型
        desired_caps['app'] = PATH(                                         #app：.apk文件所在的本地绝对路径或者远程路径，Appium会先尝试安装路径对应的应用在适当的真机或模拟器上
            '../../../sample-code/apps/ApiDemos/bin/ApiDemos-debug.apk'
        )

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    #tearDown方法在每个测试方法执行后调用，这个地方做所有清理工作，此处执行的是退出
    def tearDown(self):
        self.driver.quit()

    #定义元素查找方法
    def test_find_elements(self):                                           

        el = self.driver.find_element_by_accessibility_id('Graphics')       #定位'Graphics'项
        el.click()                                                          #点击'Graphics'
        el = self.driver.find_element_by_accessibility_id('Arcs')           #定位'Graphics/Arcs'项
        self.assertIsNotNone(el)

        self.driver.back()

        el = self.driver.find_element_by_accessibility_id("App")            #定位'APP'项
        self.assertIsNotNone(el)

        #利用android的uiautoamtor中的属性来获取new UiSelector().clickable(true)控件
        els = self.driver.find_elements_by_android_uiautomator("new UiSelector().clickable(true)")
        self.assertGreaterEqual(12, len(els))

        #利用android的uiautoamtor中的属性来获取单个控件——text("API Demos“）
        self.driver.find_element_by_android_uiautomator('text("API Demos")')

    #定义一个动作方法
    def test_simple_actions(self):
        el = self.driver.find_element_by_accessibility_id('Graphics')
        el.click()

        el = self.driver.find_element_by_accessibility_id('Arcs')
        el.click()

        self.driver.find_element_by_android_uiautomator('new UiSelector().text("Graphics/Arcs")')


if __name__ == '__main__':
    #定义一个单元测试容器，将测试用例放入测试容器中
    suite = unittest.TestLoader().loadTestsFromTestCase(SimpleAndroidTests)
    #自动进行测试
    unittest.TextTestRunner(verbosity=2).run(suite)
