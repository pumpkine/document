安装所采用的镜像为R2014b_glnxa64.ISO。破解文件分有两个，一个为libmwservices.so文件，另一个为MATLAB_R2014B_MAC_LINUX_crack文件夹[详见共享目录：`/192.168.0.227/01.专业软件/matlab/ubuntu-matlab2014b`]。安装步骤如下：

### 1.把镜像文件中的install.jar文件替换为MATLAB_R2014B_MAC_LINUX_crack中的install.jar

#### 1.将R2014b_glnxa64.ISO解压到随便某一个目录下（注意：使用ubuntu自带的工具解压，即右键->archive manager），本教程将解压后的放在`/home/lab302/matlab2014b`中

#### 2.将破解文件MATLAB_R2014B_MAC_LINUX_crack中的install.jar复制到`matlab2014b/java/jar/`下的覆盖原来的install.jar，并执行指令`chmod +x *`

#### 3.进入到java/jar文件夹下，可以看到有install.jar文件，执行命令chmod +x *。然后将该文件删除`rm install.jar`

### 2.安装matlab

回到解压后的文件目录matlab2014b/下，可以看到有intall文件，执行`sudo ./install`命令，开始安装【注意：选择使用文件安装秘钥】

### 3.输入installation key: 29797-39064-48306-32452，安装路径这里选择`/usr/local/MATLAB/R2014b`

### 4.不联网激活：添加MATLAB_R2014B_MAC_LINUX_crack下的license.lic文件激活

### 5.把libmwservices.so文件放入到`/usr/local/MATLAB/R2014B/bin/glnxa64`文件下

### 6.进入到`/usr/local/MATLAB/R2014b/bin`文件夹中，输入./matlab启动matlab。

### 7.至此安装完成。
