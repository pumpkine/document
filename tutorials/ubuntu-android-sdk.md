网盘链接：[http://pan.baidu.com/s/1c096Bi4](http://pan.baidu.com/s/1c096Bi4) 密码：batc

### 一、安装eclipse

#### 方法一：从官网下载安装包直接安装

【网盘：eclipse-standard-luna-R-linux-gtk-x86_64.tar.gz，需要配置路径】。

#### 方法二：在ubuntu terminal下输入指令sudo apt-get install eclipse

#### 方法三：在ubuntu的software center中搜索eclipse，点击“安装”直接安装

#### 注：第一次启动会出现An error has occurred. 让你查看一个log文件

解决方法：链接一个库过去，指令如下：

	ln -s /usr/lib/jni/libswt-*3740.so ~/.swt/lib/linux/x86_64/

### 二、ADT安装

#### 方法一，在线安装：

打开Eclipse→Help→Install New Software

点击Work with的“Add”

Name：ADT Online（随意）

Location：https://dl-ssl.google.com/android/eclipse/

由于GFW，ADT的安装比较困难，会提示contact with Server。

#### 方法二，手动安装：

下载ADT插件【网盘：ADT-23.0.6.zip】

点击方法一中的“Add”

Name：ADT Plugin（随意）

点击Archive...选择ADT路径

点击完成

### 三、安装Android SDK

ADT安装完成后会弹出对话框，直接选择Android SDK的路径【网盘：Android Sdk（include api）.tar.gz】即可

**注**：ubuntu-12.04-SmdtSDK-amd64.iso镜像已经提供java环境，在此不需要重新安装
 
编译文件时可能会遇到一个问题： Your project contains error(s)，please fix them before running your application.

解决方法：

	sudo apt-get install lib32z1 lib32stdc++6
	
**注**:lib32z1中最后是数字1
