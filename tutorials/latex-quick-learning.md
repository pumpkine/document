## Latex极快上手

### 调用现成的模板，比如seuthesis、IEEE conference等。

### 插入章节号

	\chapter{绪论}\label{sec.intro} % 自动会编号，章，类似1.，章索引{sec.intro}
	\section{研究背景与意义}\lable{sec.BG} % 节，类似1.1，正文可引用\ref{sec.BG}
	\subsection{移动智能软硬件系统发展} % 小节，类似1.1.1

输出PDF如下图所示：

![章节号示例](http://7xkoxp.com1.z0.glb.clouddn.com/latex-quick-learning-blog/1.jpg)
 
### 正文插入多行项目符号列表

	\begin{itemize} 
	\item 第一段文字
	\item 第二段文字
	\end{itemize}

输出PDF为：
 
![多行项目符号列表示例](http://7xkoxp.com1.z0.glb.clouddn.com/latex-quick-learning-blog/2.jpg)

### 插入引用文献

	\cite{NeilMawston:2013}

注意：最好在google scholar中找到被引用文献，然后点击链接下方的cite，如下图：

![文献引用示例1](http://7xkoxp.com1.z0.glb.clouddn.com/latex-quick-learning-blog/3.jpg)
 
然后在弹出窗口中选择BibTex，如下图所示：

![文献引用示例2](http://7xkoxp.com1.z0.glb.clouddn.com/latex-quick-learning-blog/4.jpg)
 
最后保存输出的所有文本到专门的xxx.bib文件中，正文引用时在\cite{}括号中填入红圈中的名字即可，如下：

![文献引用示例3](http://7xkoxp.com1.z0.glb.clouddn.com/latex-quick-learning-blog/5.jpg)
 
### 插入图片：
	\begin{figure}[ht]
	  \centering % 图片居中对齐
	  \centerline{\includegraphics[width = 20.3cm]{figures/fInstMix4.pdf}} %原图路径
	  \caption{图片的中/英文名称和说明，显示在图片下方}
	  \label{fig.fInstMix4} % 图片索引，在正文中用“如图\ref{fig.fInstMix4}”即“如图3.4”
	\end{figure}

图片保存到figures/这个目录下，最好采用.eps或者.pdf，这是矢量图片。如果是matlab生成图片，需要转换一下，详见matlab的使用方法。输出PDF入下图所示：

![插入图片示例1](http://7xkoxp.com1.z0.glb.clouddn.com/latex-quick-learning-blog/6.jpg)

正文中引用显示为：

![插入图片示例2](http://7xkoxp.com1.z0.glb.clouddn.com/latex-quick-learning-blog/7.jpg)

### 插入表格

	\begin{table}[ht]
	\begin{center}
	\newcommand{\tabincell}[2]{\begin{tabular}{@{}#1@{}}#2\end{tabular}}
	\caption{表格的中/英文名称和说明，显示在表格上方}\label{tab:MICAparams} % 表格索引
	\begin{tabular}{l|l} % 两列表格，均为左对齐；{c|c|r}：3列表格，前两居中最后一列右对齐
	\hline % 横线，会显示出来
	参数名称&参数含义\\ % 第1行内容，用&分割两列，“\\”表示换行；AAA&BBB&CCC表示三列
	\hline % 横线分割，也可以不要
	程序指令级并行度（ILP）&\tabincell{l}{理想处理器中，指令窗口容量\\分别为32、64、96和128时，\\可以并发执行的指令条数}\\ % 第2行内容，其中第二列分为3行显示（tabincell{l}）左对齐
	\hline
	\tabincell{l}{指令依赖关系类特征\\（寄存器依赖距离）}&\tabincell{l}{两次架构寄存器重用\\之间动态指	令条数的分布}\\ % 第3行内容，第1列和第2列均占用2行空间，均为l(左)对其
	\end{tabular}
	\end{center}
	\end{table}

输出pdf如下图所示：

![插入表格示例1](http://7xkoxp.com1.z0.glb.clouddn.com/latex-quick-learning-blog/8.jpg)

正文引用输出效果为：

![插入表格示例2](http://7xkoxp.com1.z0.glb.clouddn.com/latex-quick-learning-blog/9.jpg)
 
### 插入公式

	\begin{eqnarray}\label{eqn.cstall} % 公式索引
	  \nonumber % 多行公式共用一个索引号，就需要在不需要编号的行加这个东西
	  c_{stall} &=& max(0;1-\frac{c_{miss}}{N/D+c^{'}_{stall}})\cdot c^{'}_{stall}, \\% &=&表示多行间按“=”对齐；_{}表示下标；\frac{A}{B}表示A/B；^{}表示上标；\cdot表示乘号；
	  MLP_{avg} &=& \frac{\sum_{e}m(e)}{\sum_{e}1} % \sum表示求和符号
	\end{eqnarray}

