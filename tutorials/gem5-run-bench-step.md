### 1.安装所需工具

	sudo apt-get install git scons g++ python-dev swig m4

### 2.下载我们改过的my-gem5，下载地址如下:（已有的话就git pull更新一下）

	git clone https://tianna1121@bitbucket.org/tianna1121/my-gem5.git

如果速度过慢，采用下面地址下载

    链接: http://pan.baidu.com/s/137oku 密码: shs6

### 3.编译gem5

进入my-gem5文件夹之后

	scons build/ARM/gem5.opt

第一次较慢(大概20分钟)，如果以前跑过gem5可以备份一下build文件夹之后再编译

### 4.下载Linux镜像：

	http://www.gem5.org/dist/current/arm/aarch-system-2014-10.tar.xz

### 5.解压aarch-system-2014-10.tar.xz文件到my-gem5文件夹下的img文件夹(如果没有则自己创建)

目录结构如下：

> gem5/

> |----img/

> |----|----binaries/*

> |----|----disks/*

### 6.下载benchmark，放到文件系统镜像(此镜像作为linux文件系统)里

下载地址

    链接: http://pan.baidu.com/s/1qWJZF0S 密码: ms7h

向镜像添加文件指令

    sudo mount -o loop,offset=32256 img/disks/linux-aarch32-ael.img /mnt
    cp -a Mibench /mnt
    cp -a mediaBench /mnt
	cd /mnt
	chmod +x /mediabench/* -r
	chmod +x /Mibench/* -r
    umount /mnt

### 7.构建仿真目录

将gem5/build/ARM/gem5.opt和gem5/configs/链接到任一文件夹下（这里采用gem5/parsec-1/)，开始仿真,**注意**为不同的benchmark创建不同的目录

链接指令如下

    进入parsec-1目录
    ln -s ../build/ARM/gem5.opt ./
    ln -s ../configs/ ./

目录结构如下：

> parsec-1/

> |----board_start.sh

> |----gem5.opt

> |----configs/

### 8.启动Linux系统(gem5目录下)，设置观察点：

仿真的过程首先启动系统，然后在系统中跑bench，为节省时间可以分两步：

#### 1. 用AtomicSimpleCpu模式启动linux， 设置一个观察点

修改board_start.sh文件:

> 1.首先将`M5_PATH`修改为你自己的img路径

> 2.可能需要根据不同的模式进行相应的修改，且注意每种模式后面参数文件是否存在

运行`./board_start.sh`时可能会出现错误，提示有两个文件不存在：

> gem5/img/disks/sdcard-1g-mxplayer.img和gem5/img/binaries/vmlinux-gem5-android-dvfs，从上述网盘可以下载，放入相应的路径下即可

运行`gem5/parsec-1/`目录下`board_start.sh`

再开一个终端，运行`./util/term/m5term 127.0.0.1 3456` 这里的`3456`是个端口号，开第二个的话就是`3457`（gem5说明：会自动变化）

    AEL login:root

出现`#`号时，说明linux系统已经启动好

    m5 checkpoint       #设置观察点
    m5 exit             #设置完成退出，不用关闭窗口 

此时parserc-1/m5out/文件夹里会生成cpt.xxxxx文件夹，即为checkpoint，这个文件夹可以重复使用，跑另一个bench的时候复制其链接到m5out文件夹中

#### 2. 用arm_detailed模式从设置的观察点继续执行 

再次运行`./util/term/m5term 127.0.0.1 3456`就可以看到所跑linux系统的命令行`#`，即已经进入m5终端

### 9.跑具体的bench

进入系统后，根据具体的bench指令直接执行应用

**注意：**m5终端下尽量少使用bash指令（比如ls, cd, chmod等等，此会影响所跑应用的状态结果，最好提前修改权限一句指令解决）

### 10.保存bench运行信息

进入parsec-1/m5out/Mibench/，复制一份qsort_small为你所跑bench的名字，然后删除qsort_small下去掉检查点文件cpt.xxxxxxx以外的所有文件，重新开启第8.2步跑新的bench

END

AtomicSimpeCPU模式：

    ./gem5.opt -d m5out/Mibench/qsort_small/ --debug-flags=O3PipeView configs/example/fs.py --mem-size=1024MB --kernel=vmlinux-gem5-android-dvfs --disk-image=linux-aarch32-ael.img --caches --l1i_size=32kB --l1d_size=32kB --l1d_assoc=2 --l1i_assoc=2 --l2_assoc=16 --l2cache --l2_size=128kB --num-l2caches=8 --cpu-type=AtomicSimpleCPU -n 1 --machine-type=VExpress_EMM --dtb-filename=vexpress-v2p-ca15-tc1-gem5_dvfs_1cpus.dtb --frame-capture --enable-context-switch-stats-dump

arm_detailed模式：

    ./gem5.opt -d m5out/Mibench/qsort_small/ --debug-flags=O3PipeView configs/example/fs.py --mem-size=1024MB --kernel=vmlinux-gem5-android-dvfs --disk-image=linux-aarch32-ael.img --caches --l1i_size=32kB --l1d_size=32kB --l1d_assoc=2 --l1i_assoc=2 --l2_assoc=16 --l2cache --l2_size=128kB --num-l2caches=8 --cpu-type=arm_detailed -n 1 --machine-type=VExpress_EMM --dtb-filename=vexpress-v2p-ca15-tc1-gem5_dvfs_1cpus.dtb --frame-capture --enable-context-switch-stats-dump -r 1

**注意：**不同的配置信息后加入`-r 1`代表从观察点启动，没有则表示直接启动，可以设置观察点
