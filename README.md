## 说明

站在前人的肩膀上才能看的更远。

[本文档管理地址](https://bitbucket.org/pumpkine/document/src)

## 我们在这里积累有意思的教程可好？

[Markdown示例文档](http://maxiang.info/)

[有意思的理解git教程](http://pcottle.github.io/learnGitBranching)

[是不是应该向他学习](https://bitbucket.org/ChenGuangXing/blog/wiki/Home)
